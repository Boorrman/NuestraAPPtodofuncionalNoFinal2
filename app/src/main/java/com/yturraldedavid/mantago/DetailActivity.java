package com.yturraldedavid.mantago;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.media.Image;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMapOptions;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.ArrayList;

public class DetailActivity extends AppCompatActivity implements OnMapReadyCallback{

    ImageView imageViewLugar;
    TextView textViewLugar;
    TextView textViewDescripcion;
    GoogleMap map;
    Button botonMapa;

    GoogleMapOptions options = new GoogleMapOptions();
    private GoogleMap mMap;




    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);


        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getApplicationContext());

        if(status == ConnectionResult.SUCCESS){

            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.anotherMap);
            mapFragment.getMapAsync(this);
        }else{
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, (Activity)getApplicationContext(),10);
            dialog.show();
        }





        textViewLugar = (TextView) findViewById(R.id.textViewLugar);
        textViewDescripcion = (TextView) findViewById(R.id.textViewDescripcion);
        imageViewLugar = (ImageView) findViewById(R.id.imageViewLugar);
        botonMapa = (Button) findViewById(R.id.botonMapa);







        int opcion = getIntent().getExtras().getInt("ID");
        if (opcion == 0){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));


        } else if (opcion == 1){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 2){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 3){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 4){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 5){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 6){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 7){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        } else if (opcion == 8){
            imageViewLugar.setImageDrawable(getResources().getDrawable(getIntent().getExtras().getInt("foto")));
            textViewDescripcion.setText(getIntent().getExtras().getString("descripcion"));
            textViewLugar.setText(getIntent().getExtras().getString("nombreLugar"));
        }

        botonMapa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailActivity.this, MapsActivity.class);

                startActivity(intent);
            }
        });

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {

        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        UiSettings uiSettings = mMap.getUiSettings();
        uiSettings.setZoomControlsEnabled(true);

        double Lat = getIntent().getExtras().getDouble("Lat");
        double Lng = getIntent().getExtras().getDouble("Lng");
        String Nombre = getIntent().getExtras().getString("nombreLugar");

        LatLng zona = new LatLng(Lat, Lng);

        Log.e("Latitud", String.valueOf( zona));



        mMap.addMarker(new MarkerOptions().position(zona).title(Nombre)
            .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));

        float zoomlevel=15;
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(zona,zoomlevel));


    }




}
