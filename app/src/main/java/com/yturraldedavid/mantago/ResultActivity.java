package com.yturraldedavid.mantago;

import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class ResultActivity extends AppCompatActivity implements  RecyclerView.OnItemTouchListener{

    RecyclerView recyclerViewLugares;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);
        recyclerViewLugares = (RecyclerView) findViewById(R.id.recyclerViewLugares);
        recyclerViewLugares.setLayoutManager(new LinearLayoutManager(this));
        final ArrayList<LugaresTuristicos> listaLugares = new ArrayList<>();
        final ArrayList<MapaLugar> latlng = new ArrayList<>();
        int opcion = getIntent().getExtras().getInt("opcion");
        switch (opcion){
            case 1:
                listaLugares.add(new LugaresTuristicos("Museo Cancebí",R.drawable.lt01, "Museo caracteristico de manabi", 0));
                latlng.add(new MapaLugar(-0.947417, -80.721746));
                break;
            case 2:
                listaLugares.add(new LugaresTuristicos("Playa Murcielago", R.drawable.p01,"Playa principal de la ciudad", 1));
                latlng.add(new MapaLugar( -0.939401, -80.730193));
                listaLugares.add(new LugaresTuristicos("Playita Mia", R.drawable.p02,"Playa característica por su venta de pescados", 2));
                latlng.add(new MapaLugar(-0.949447, -80.709220));
                break;

            case 3:
                listaLugares.add(new LugaresTuristicos("Martinica", R.drawable.r01,"Restaurante con un toque fino ", 3));
                latlng.add(new MapaLugar(-0.945878, -80.745170));
                listaLugares.add(new LugaresTuristicos("Chamaco", R.drawable.r02,"Restaurante enfocado a la comida mexicana", 4));
                latlng.add(new MapaLugar(-0.946947,-80.745965));
                break;
            case 4:
                listaLugares.add(new LugaresTuristicos("Wild Bar", R.drawable.b01,"Este bar es frecuentado por gente joven", 5));
                latlng.add(new MapaLugar(-0.945044, -80.735862));
                listaLugares.add(new LugaresTuristicos("Noa Bar", R.drawable.b02,"Bar capaz de otorgar una musica agradable", 6));
                latlng.add(new MapaLugar(-0.945188, -80.731077));
                break;
            case 5:
                listaLugares.add(new LugaresTuristicos("Oro verde", R.drawable.h01,"Hotel mas conocido de la ciudad", 7));
                latlng.add(new MapaLugar(-0.941511, -80.732737));
                listaLugares.add(new LugaresTuristicos("Balandra", R.drawable.h02,"Hotel barato y comodo", 8));
                latlng.add(new MapaLugar(-0.942486, -80.730536));

        }


        
        ResultListAdapter adapter = new ResultListAdapter(listaLugares);
        adapter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int id= listaLugares.get
                        (recyclerViewLugares.getChildAdapterPosition(v))
                        .getId();

                if (id == 0) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 1) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 2) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 3) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                } else if (id == 4) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 5) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 6) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 7) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }else if (id == 8) {

                    Intent intent = new Intent(ResultActivity.this, DetailActivity.class);
                    intent.putExtra("ID", id);
                    intent.putExtra("nombreLugar", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getNombre());
                    intent.putExtra("descripcion", listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getDescripcion());
                    intent.putExtra("foto",  listaLugares.get(recyclerViewLugares.getChildAdapterPosition(v)).getFoto());
                    intent.putExtra("Lat", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLat());
                    intent.putExtra("Lng", latlng.get(recyclerViewLugares.getChildAdapterPosition(v)).getLng());
                    startActivity(intent);
                }










                Toast.makeText(getApplicationContext(), "Selección: "+listaLugares.get
                        (recyclerViewLugares.getChildAdapterPosition(v))
                        .getNombre(),Toast.LENGTH_SHORT).show();
            }
        });
        recyclerViewLugares.setAdapter(adapter);

    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }

    public interface  OnItemClickListener {
        public void OnItemClickListener(View view, int position);


    }



}
